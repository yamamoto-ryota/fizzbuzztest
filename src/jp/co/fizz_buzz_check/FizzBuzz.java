package jp.co.fizz_buzz_check;

public class FizzBuzz {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		// 挙動の確認用に1〜100までの整数を引数にして「checkFizzBuzz」を呼び出す
        for(int i = 1; i <= 100; i++) {
            System.out.println(checkFizzBuzz(i));
        }
    }

    /*
     * 引数numが3の倍数なら「Fizz」
     * 5の倍数なら「Buzz」
     * 3と5両方の倍数なら「FizzBuzz」
     * それ以外ならそのまま数字を戻り値として返す
     *
     * 引数 int
     * 戻り値 String
     */
    public static String checkFizzBuzz(int num) {

    	String strFizzBuzz = "FizzBuzz";
    	String strFizz = "Fizz";
    	String strBuzz = "Buzz";

        	if(((num %3) == 0) && ((num %5) == 0)){
        		return strFizzBuzz;

        	}else if((num %3) == 0) {
        		return strFizz;

        	}else if((num %5) == 0) {
        		return strBuzz;

        	}else {
        		String Num = String.valueOf(num);
        		return Num;

        	}
        }
 }



