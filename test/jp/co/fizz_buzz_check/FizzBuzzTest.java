/**
 *
 */
package jp.co.fizz_buzz_check;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author yamamoto.ryota
 *
 */
public class FizzBuzzTest {


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}


	/**
	 * {@link jp.co.fizz_buzz_check.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_01() {
		System.out.println("testCheckFizzBuzz_01");
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	/**
	 * {@link jp.co.fizz_buzz_check.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_02() {
		System.out.println("testCheckFizzBuzz_02");
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	/**
	 * {@link jp.co.fizz_buzz_check.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_03() {
		System.out.println("testCheckFizzBuzz_03");
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	/**
	 * {@link jp.co.fizz_buzz_check.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_04() {
		System.out.println("testCheckFizzBuzz_04");
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	/**
	 * {@link jp.co.fizz_buzz_check.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzz_05() {
		System.out.println("testCheckFizzBuzz_05");
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}

}
